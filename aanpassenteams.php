<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';

// Session klassen aanroepen en init
require_once 'tools/Session.php';
$session = new Session();

require_once 'tools/Messages.php';
$messages = new Messages();

$mysqli = get_mysqli();

$teamId = 0;
if (isset($_GET['wedstrijdid'])) {
    $teamId = $_GET['wedstrijdid'];
}

$sql = "SELECT * FROM team WHERE id = " . $teamId;
$result = $mysqli->query($sql);
$team = $result->fetch_assoc();

//Team informatie ophalen uit de database

$teamId = $team['id'];
$teamKlasse = $team['klasse'];
$teamNaam = $team['naam'];

//Zodra de gebruiker op de knop submit heeft geklikt dan...
if (isset($_POST['submit'])) {

    //updaten
    $statement = $mysqli->prepare("UPDATE team SET klasse = ?, naam = ? WHERE id = ?");
    //succes melding
    $statement->bind_param('ssi', $_POST['team_klasse'], $_POST['team_naam'], $_POST['team_id']);
    $results = $statement->execute();

    if ($results) {

        $sql = "SELECT * FROM team WHERE id = " . $teamId;
        $result = $mysqli->query($sql);
        $team = $result->fetch_assoc();

        $teamId = $team['id'];
        $teamKlasse = $team['klasse'];
        $teamNaam = $team['naam'];

        $session->flashSession('team_bewerken', $teamNaam . " is aangepast in het systeem.");

    } else {

        print 'Error : (' . $mysqli->errno . ') ' . $mysqli->error;

    }

}

$sqlKlas = "SELECT * FROM klas ORDER by code ASC";
$resultKlas = $mysqli->query($sqlKlas);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>
<main class="container">

    <?= $messages->alertMessage('success', $session->flashSession('team_bewerken')); ?>

    <div class="well"><h1>Aanpassen teams</h1></div>

    <form class="" action="" method="post">

        <input type="hidden" name="team_id" value="<?php echo $teamId; ?>">

        <table class="table table-condensed table-striped">

            <tr>
                <th>Klasse</th>
                <th>Naam</th>
            </tr>

            <tr>
                <td>

                    <select name="team_klasse" id="klasse">

                        <?php while ($row = $resultKlas->fetch_assoc()) { ?>

                            <?php if ($teamKlasse == $row['code']): ?>
                                <option selected value="<?= $row['code']; ?>"><?= $row['naam']; ?></option>
                            <?php endif; ?>

                            <option value="<?= $row['code']; ?>"><?= $row['naam']; ?></option>
                        <?php } ?>

                    </select>

                </td>
                <td>
                    <input type="text" name="team_naam" value="<?php echo $teamNaam; ?>">
                </td>
            </tr>

        </table>

        <input type="submit" name="submit" value="Verzenden">

    </form>

</main>
</body>
</html>
