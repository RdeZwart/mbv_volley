<?php
// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // even gemakkelijk, zonder validatie of andere checks
    // PAS OP!!! Gevaarlijk voor SQL-injectie of andere aanvallen en foutgevoelig
    $wedstrijdid = $_POST['wedstrijdid'];

    // Wie is team A?
    $sql = "SELECT * FROM wedstrijd WHERE id=" . $wedstrijdid;
    $result = $mysqli->query($sql);
    if ($result->num_rows == 0) {
        die("Wedstrijd wedstrijdid is niet bekend");
    }
    $wedstrijddata = $result->fetch_assoc();

    //each set in POST
    for ($set = 1; $set <= 4; $set++) {
        if (strlen($_POST['S' . $set . 'Sa']) > 0 &&
                strlen($_POST['S' . $set . 'Sa']) > 0 &&
                strlen($_POST['S' . $set . 'Sa']) > 0 &&
                strlen($_POST['S' . $set . 'Sa']) > 0) {
            // overnemen van de data in lokale variabelen
            $score_a = $_POST['S' . $set . 'Sa'];
            $score_b = $_POST['S' . $set . 'Sb'];
            $punten_a = $_POST['S' . $set . 'Pa'];
            $punten_b = $_POST['S' . $set . 'Pb'];
            // opslaan in de database
            $sql = "UPDATE uitslag_set SET score_a='$score_a', score_b='$score_b', punten_a='$punten_a', punten_b='$punten_b' WHERE wedstrijd = $wedstrijdid AND `set` = $set";
            $result = $mysqli->query($sql);
        }
    }
    // relocate with message on update
    if ($query_does_not_execute) {
        $errcode = "error_code=003";
    }

    $referer = $_SERVER['HTTP_REFERER'];
    $message = "succes message";


    if ($errcode) {
        if (strpos($referer, '?') === false) {
            $referer .= "?";
        }

        header("Location: $referer&$errcode");
    } else {
        header("Location: $referer&message=$message");
    }
    exit;
}

$teamid = 0;
if (isset($_GET['wedstrijdid'])) {
    $wedstrijdid = $_GET['wedstrijdid'];
    $sql = "SELECT w.*, s.datum, r.tijd  FROM wedstrijd w 
                INNER JOIN speelweek s ON s.id=w.speelweek 
                INNER JOIN ronde r ON r.id=w.ronde WHERE w.id=$wedstrijdid";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
        $wedstrijddata = $result->fetch_assoc();
        // Haal gegevens van team a er bij
        $team_a_id = $wedstrijddata['team_a'];
        $team_a = $mysqli->query("SELECT * FROM team WHERE id=$team_a_id")->fetch_assoc();
        // ... en stop dit in het resultaat array
        $wedstrijddata['teama'] = $team_a['naam'];
        // Samen met de klasse (is voor alle drie de teams gelijk)
        $wedstrijddata['klasse'] = $team_a['klasse'];
        // Haal gegevens van team b er bij ...
        $team_b_id = $wedstrijddata['team_b'];
        $team_b = $mysqli->query("SELECT * FROM team WHERE id=$team_b_id")->fetch_assoc();
        // ... en stop dit in het resultaat array
        $wedstrijddata['teamb'] = $team_b['naam'];
        // Haal gegevens van het scheidsrechter team er bij ...
        $team_s_id = $wedstrijddata['scheids'];
        $team_s = $mysqli->query("SELECT * FROM team WHERE id=$team_s_id")->fetch_assoc();
        // ... en stop dit in het resultaat array
        $wedstrijddata['teams'] = $team_s['naam'];
    }
}


// Ophalen uitslagen
$sqlUitslagId = "SELECT wedstrijd FROM uitslag_set GROUP BY wedstrijd";
$resId = $mysqli->query($sqlUitslagId);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MBV Volley</title>
        <?php include 'head.html' ?>		
    </head>
    <body>
        <?php include 'header.php' ?>
        <main class="container">	
            <div class="well"><h1>Invullen uitslag</h1></div>
            <?php if (isset($_GET['message'])) { ?>
                <div class="mymessage alert alert-success">
                    <?php echo "Uw aanpassingen zijn verwerkt"; ?>
                </div>
            <?php } ?>
            <form method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-4"><strong>DATUM:</strong> <?php echo $wedstrijddata['datum'] ?></div>
                            <div class="col-xs-4"><strong>TIJD:</strong> <?php echo $wedstrijddata['tijd'] ?></div>
                            <div class="col-xs-2"><strong>VELD:</strong> <?php echo $wedstrijddata['veld'] ?></div>
                            <div class="col-xs-2"><strong>KLAS:</strong> <?php echo $wedstrijddata['klasse'] ?></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-xs-12"><strong>SCHEIDSRECHTER:</strong> <?php echo $wedstrijddata['teams'] ?></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-xs-6"><strong>TEAM A:</strong> <?php echo $wedstrijddata['teama'] ?></div>
                            <div class="col-xs-6"><strong>TEAM B:</strong> <?php echo $wedstrijddata['teamb'] ?></div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="wedstrijdid" value="<?php echo $wedstrijddata['id'] ?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th rowspan="2">SET</th>
                                    <th colspan="2">SCORE</th>
                                    <th colspan="2">PUNTEN</th>
                                </tr>
                                <tr>
                                    <th>TEAM A</th>
                                    <th>TEAM B</th>
                                    <th>TEAM A</th>
                                    <th>TEAM B</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //count for all input fields based on set
                                $counter = 0;
                                $sql = "SELECT * FROM uitslag_set WHERE wedstrijd = $wedstrijdid";
                                $resUitslag = $mysqli->query($sql);
                                while ($uitslag = $resUitslag->fetch_assoc()) {
                                    $counter++;
                                    ?>
                                    <tr>
                                        <th><strong><?php echo $counter; ?></strong></th>
                                        <th><input type="number" onkeydown="javascript: return event.keyCode == 69 ? false : true" onpaste="handlePaste(event)"  class="form-control" name="S<?php echo $counter; ?>Sa" value="<?php echo $uitslag['score_a']; ?>"></th>
                                        <th><input type="number" onkeydown="javascript: return event.keyCode == 69 ? false : true" onpaste="handlePaste(event)"  class="form-control" name="S<?php echo $counter; ?>Sb" value="<?php echo $uitslag['score_b']; ?>"></th>
                                        <th><input type="number" onkeydown="javascript: return event.keyCode == 69 ? false : true" onpaste="handlePaste(event)"  class="form-control" name="S<?php echo $counter; ?>Pa" value="<?php echo $uitslag['punten_a']; ?>"></th>
                                        <th><input type="number" onkeydown="javascript: return event.keyCode == 69 ? false : true" onpaste="handlePaste(event)"  class="form-control" name="S<?php echo $counter; ?>Pb" value="<?php echo $uitslag['punten_b']; ?>"></th>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary">Aanpassen</button>
                    </div>
                </div>                
            </form>
            <script>
                //Removing 'e' from numeric type
                function FilterInput(event) {
                    var keyCode = ('which' in event) ? event.which : event.keyCode;

                    isNotWanted = (keyCode == 69 || keyCode == 101);
                    return !isNotWanted;
                }
                ;
                function handlePaste(e) {
                    var clipboardData, pastedData;

                    // Get pasted data via clipboard API
                    clipboardData = e.clipboardData || window.clipboardData;
                    pastedData = clipboardData.getData('Text').toUpperCase();

                    if (pastedData.indexOf('E') > -1) {
                        //alert('found an E');
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }
                ;
            </script>
        </main>
    </body>
</html>
