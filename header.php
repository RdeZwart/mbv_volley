<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Trucje om de huidige pagina te bepalen
// Met de huidige pagina bedoel ik de naam van het (php) bestand dat op dit moment wordt 'uitgevoerd'
// de functie basename haalt de naam van het bestand uit een pad met eventuele subfolders
$page = basename($_SERVER["PHP_SELF"]);

if (isAuthenticated()) {
    // Hier kunnen extra menu-items worden toegevoegd die alleen beschikbaar zijn voor een ingelogde gebruiker

    $menu_items = array(
        array(
            "Name" => "Over ons",
            "URL" => "about.php"
        ),
        array(
            "Name" => "Indeling competitie",
            "URL" => "indeling_competitie.php"
        ),

        array(
            "Wedstrijd" => array(
                array(
                    "Name" => "Wedstrijdschema",
                    "URL" => "wedstrijdschema.php"
                ),
                array(
                    "Name" => "Uitslagen",
                    "URL" => "uitslagen.php"
                ),
                array(
                    "Name" => "Standen",
                    "URL" => "standen.php"
                ),
                array(
                    "Name" => "Schema Genereren",
                    "URL" => "wedstrijdgenereren.php"
                )
            )
        ),

        array(
            "Beheer" => array(
                array(
                    "Name" => "Teams",
                    "URL" => "teamread.php"
                ),
                array(
                    "Name" => "Klasse",
                    "URL" => "klassen.php"
                ),
                array(
                    "Name" => "Leden",
                    "URL" => "leden.php"
                ),
            )
        ),

        array(
            "Name" => "Contact",
            "URL" => "contact.php"
        )


    );

} else {

    // Hier kunnen extra menu items worden toegevoegd die alleen beschikbaar zijn voor een niet ingelogde gebruiker
    $menu_items = array(
        array(
            "Name" => "Over ons",
            "URL" => "about.php"
        ),
        array(
            "Name" => "Indeling competitie",
            "URL" => "indeling_competitie.php"
        ),

        array(
            "Wedstrijd" => array(
                array(
                    "Name" => "Wedstrijdschema",
                    "URL" => "wedstrijdschema.php"
                ),
                array(
                    "Name" => "Uitslagen",
                    "URL" => "uitslagen.php"
                ),
                array(
                    "Name" => "Standen",
                    "URL" => "standen.php"
                ),
            )
        ),

        array(
            "Name" => "Contact",
            "URL" => "contact.php"
        )
    );

}

?>

<header>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

        <div class="container-fluid container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-bars fa-inverse"></i>
                </button>

                <a class="navbar-brand" href="index.php">MBV<i class="fa fa-futbol-o"></i>lley</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav">
                    <?php foreach ($menu_items as $menu_item): ?>

                        <?php if (key($menu_item) !== "Name"): ?>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><?= key($menu_item); ?> <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">

                                    <?php foreach ($menu_item as $dropdown): ?>
                                        <?php foreach ($dropdown as $dropdown_menu_item): ?>

                                            <?php $class = $dropdown_menu_item["URL"] === $page ? "active" : ""; ?>
                                            <li class="<?= $class; ?>">
                                                <a href="<?= $dropdown_menu_item['URL']; ?>"><?= $dropdown_menu_item['Name']; ?></a>
                                            </li>

                                        <?php endforeach; ?>
                                    <?php endforeach; ?>

                                </ul>
                            </li>

                        <?php else: ?>

                            <?php $class = $menu_item["URL"] === $page ? "active" : ""; ?>
                            <li class="<?= $class; ?>">
                                <a href="<?= $menu_item['URL']; ?>"><?= $menu_item['Name']; ?></a>
                            </li>

                        <?php endif; ?>

                    <?php endforeach; ?>
                </ul>

                <ul class="nav navbar-nav navbar-right">

                    <?php if (isset($_SESSION['loggedin'])): ?>

                        <?php $username = isset($_SESSION['username']) ? $_SESSION['username'] : 'Onbekend'; ?>

                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                <i class="fa fa-user"></i>&nbsp;<?php echo $username; ?>&nbsp;<i
                                        class="fa fa-caret-down"></i>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li class="menu_item">
                                    <a href="login.php?action=logoff">
                                        <i class="fa fa-power-off"></i> Afmelden
                                    </a>
                                </li>
                            </ul>

                        </li>

                    <?php else: ?>

                        <li class="menu_item"><a href="login.php?action=login">Log in</a></li>

                    <?php endif; ?>
                </ul>

            </div><!-- /.navbar-collapse -->

        </div><!-- /.container-fluid -->

    </nav>
</header>