<?php

// Sessies en Autorisatie
require_once 'tools/security.php';

if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

// Haal het team ID uit het HTTP request
$klasId = '';
if (isset($_GET['klasid'])) {
    $klasId = "'" . $_GET['klasid'] . "'";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>

<main class="container">

    <div class="well">
        <?php
        $sql = "SELECT * FROM klas WHERE code = " . $klasId;

        $result = $mysqli->query($sql);

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $klasNaam = $row['naam'];
            echo '<h1>Klasse ' . $klasNaam . '</h1>';
        }
        ?>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">

                <div class="col-md-12">
                    <?php

                    $sqlKlas = "SELECT * FROM klas WHERE code = " . $klasId;

                    if ($result = $mysqli->query($sqlKlas)) {

                        /* fetch associative array */
                        ?>

                        <table class="table">
                            <thead>
                            <tr>
                                <th>
                                    Code
                                </th>
                                <th>
                                    Naam
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php while ($row = $result->fetch_assoc()) { ?>

                                <tr>
                                    <td>
                                        <?= $row['code']; ?>
                                    </td>
                                    <td>
                                        <?= $row['naam']; ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>


                        <?php
                        /* free result set */
                        $result->free();

                    } else {
                        echo 'Geen resultaat.';
                    }
                    ?>

                </div>

            </div>

        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <a class="btn btn-default" href="" onclick="window.history.go(-1); return false;">Terug</a>
        </div>
        <div class="col-md-6">
            <div class="text-right">
                <a href="klas_bewerken.php?klasid=<?php echo $klasId; ?>" class="btn btn-warning">Wijzigen</a>
                <a href="klas_verwijderen.php?klasid=<?php echo $klasId; ?>" class="btn btn-danger">Verwijderen</a>
            </div>
        </div>

    </div>

</main>
</body>
</html>
