<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

// Session klassen aanroepen en init
require_once 'tools/Session.php';
$session = new Session();

require_once 'tools/Messages.php';
$messages = new Messages();

//Klasse registreren
if (isset($_POST['submit'])) {

    $formCode = strip_tags(trim($_POST['code']));
    $formName = strip_tags(trim($_POST['name']));

    $sql = "INSERT INTO klas (code, naam) VALUES (?, ?)";

    /* Statement voorbereiden */
    $stmt = $mysqli->prepare($sql);

    /* Bind parameters */
    $stmt->bind_param('ss', $formCode, $formName);

    /* Execute statement */
    if ($stmt->execute()) {

        $session->flashSession('klas_toevoegen', $formCode . " klasse is toegevoegd.");
        $stmt->close();

    } else {
        echo "ERROR: " . mysqli_error($mysqli);
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>
<main class="container">

    <?= $messages->alertMessage('success', $session->flashSession('klas_toevoegen')); ?>

    <div class="well">
        <h1>Klasse registreren</h1>
        <p>Voeg een klasse toe.</p>
    </div>

    <form role="form" method="post" action="">

        <div class="row">
            <div class="col-xs-3">
                <div class="form-group">
                    <label for="code">Code</label>
                    <input type="text" name="code" class="form-control" id="code">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <div class="form-group">
                    <label for="name">Naam</label>
                    <input type="text" name="name" class="form-control" id="name">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <button type="submit" name="submit" class="btn btn-success">Klasse registreren</button>
            </div>
        </div>

    </form>

</main>
</body>
</html>
