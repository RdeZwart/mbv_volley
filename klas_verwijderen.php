<?php

// Sessies en Autorisatie
require_once 'tools/security.php';

if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';

require_once 'tools/session.php';
$session = new Session();

$mysqli = get_mysqli();

// Haal het team ID uit het HTTP request
$klasId = '';
if (isset($_GET['klasid'])) {
    $klasId = $_GET['klasid'];
}

//Controleren of klassen voorkomt in de teams tabel
$sqlTeams = "SELECT * FROM team WHERE klasse = " . $klasId;
$resultTeams = $mysqli->query($sqlTeams);

if ($resultTeams->num_rows === 0) {

    //desbetreffende klas verwijderen
    $sql = "DELETE FROM klas WHERE code = " . $klasId;
    $deleteKlasQuery = mysqli_query($mysqli, $sql);

    if ($deleteKlasQuery) {

        //Melding zetten in session en deze tonen op lede.php
        $session->flashSession('klas_verwijderd', "Klas " . $klasId . " is verwijderd.");
        //Gebruiker terug sturen naar leden.php
        header('Location: klassen.php');
        exit;

    } else {
        echo "ERROR: verwijderen niet gelukt." . mysqli_error($mysqli);
    }

} else {
    //Melding zetten in session en deze tonen op lede.php
    $session->flashSession('klas_verwijderd', "Klas " . $klasId . " wordt nog gebruikt in een of meerdere team(s). Koppel de team(s) los van deze klassen en probeer het opnieuw.");
    //Gebruiker terug sturen naar klassen.php
    header('Location: klassen.php');
    exit;
}

mysqli_close($mysqli);