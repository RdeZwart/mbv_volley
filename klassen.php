<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';

// Session klassen aanroepen en init
require_once 'tools/Session.php';
$session = new Session();

require_once 'tools/Messages.php';
$messages = new Messages();

$mysqli = get_mysqli();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <!-- css en javascript inladen -->
    <?php include 'head.html' ?>
</head>
<body>
<!-- hoofdmenu inladen -->
<?php include 'header.php' ?>

<main class="container">

    <?= $messages->alertMessage('success', $session->flashSession('klas_verwijderd')); ?>

    <div class="well">
        <h1>Klassen</h1>
        <p>Een overzicht van alle klassen</p>
        <a href="klas_toevoegen.php" type="button" class="btn btn-default">Klas toevoegen</a>
    </div>

    <?php

    //Alle leden ophalen uit de database en sorteren op id van laag naar hoog.
    $sql = "SELECT * FROM klas ORDER by code ASC";

    //Controleren of er resultaat is en deze stoppen in de variabel $result.
    if ($result = $mysqli->query($sql)) {

        /* fetch associative array */
        ?>

        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>
                    Code
                </th>
                <th>
                    Naam
                </th>
            </tr>
            </thead>
            <tbody>

            <!-- De informatie die wij terug hebben gekregen in een arary loopen -->
            <?php while ($row = $result->fetch_assoc()) { ?>
                <tr>
                    <td>
                        <?= $row['code']; ?>
                    </td>
                    <td>
                        <a href="klas.php?klasid=<?= $row['code']; ?>"><?= $row['naam']; ?></a>
                    </td>
                </tr>
            <?php } ?>

            </tbody>
        </table>


        <?php
        /* free result set */
        $result->free();

    } else {
        //Zodra de query geen resultaat terug stuurt krijgt de gebruiker de volgende melding te zien:
        echo 'Geen resultaat.';
    }

    //De connectie sluiten met de database.
    $mysqli->close();
    ?>

</main>
</body>
</html>
