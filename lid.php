<?php

// Sessies en Autorisatie
require_once 'tools/security.php';

if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

// Haal het team ID uit het HTTP request
$lidId = 0;
if (isset($_GET['lidid'])) {
    $lidId = $_GET['lidid'];
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>

<main class="container">

    <div class="well">
        <?php
        $sql = "SELECT * FROM lid WHERE id = " . $lidId;

        $result = $mysqli->query($sql);

        if ($result->num_rows > 0) {

            $row = $result->fetch_assoc();
            $lidNaam = $row['naam'];

            echo '<h1>Lid ' . $lidNaam . '</h1>';
        }
        ?>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">

                <div class="col-md-8">
                    <?php

                    $sqlLid = "SELECT * FROM lid WHERE id = " . $lidId;

                    if ($result = $mysqli->query($sqlLid)) {

                        /* fetch associative array */
                        ?>

                        <table class="table">
                            <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Naam
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php while ($row = $result->fetch_assoc()) { ?>

                                <tr>
                                    <td>
                                        <?= $row['id']; ?>
                                    </td>
                                    <td>
                                        <?= $row['naam']; ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>


                        <?php
                        /* free result set */
                        $result->free();

                    } else {
                        echo 'Geen resultaat.';
                    }
                    ?>

                </div>

                <div class="col-md-4">

                    <?php
                    $sqlLidTeam = "SELECT l.id, l.naam as lidnaam, t.klasse, t.naam as teamnaam FROM `team_has_lid` as tl INNER JOIN lid as l ON tl.lid = l.id INNER JOIN team as t ON tl.team = t.id WHERE tl.lid = " . $lidId;

                    if ($result = $mysqli->query($sqlLidTeam)) {

                        /* fetch associative array */
                        ?>

                        <table class="table">
                            <thead>
                            <tr>
                                <th>
                                    Teams
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php while ($row = $result->fetch_assoc()) { ?>

                                <tr>
                                    <td>
                                        <?= $row['teamnaam']; ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>


                        <?php
                        /* free result set */
                        $result->free();

                    } else {
                        echo 'Geen resultaat.';
                    }

                    /* close connection */
                    $mysqli->close();

                    ?>

                </div>
            </div>

        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <a class="btn btn-default" href="" onclick="window.history.go(-1); return false;">Terug</a>
        </div>

        <div class="col-md-6">
            <div class="text-right">
                <a href="lid_bewerken.php?lidid=<?php echo $lidId; ?>" class="btn btn-warning">Wijzigen</a>
                <a href="lid_verwijderen.php?lidid=<?php echo $lidId; ?>" class="btn btn-danger">Verwijderen</a>
            </div>
        </div>

    </div>

</main>
</body>
</html>
