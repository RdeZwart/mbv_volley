<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';

// Session klassen aanroepen en init
require_once 'tools/Session.php';
$session = new Session();

require_once 'tools/Messages.php';
$messages = new Messages();

$mysqli = get_mysqli();

// Haal het team ID uit het HTTP request
$lidId = 0;
if (isset($_GET['lidid'])) {
    $lidId = $_GET['lidid'];
}

//Lid ophalen
$sql = "SELECT id, naam FROM lid WHERE id=$lidId";
$result = $mysqli->query($sql);
$row = $result->fetch_assoc();


//Zodra de gebruiker op de submit knop klikt gaat het volgende in actie:
if (isset($_POST['submit'])) {
    $statement = $mysqli->prepare("UPDATE lid SET naam=? WHERE id = " . $lidId);

    //bind parameters for markers, where (s = string, i = integer, d = double, b = blob) 

    $statement->bind_param('s', $_POST['name']);
    //als de naam gewijzigd is word deze opgeslagen
    $results = $statement->execute();

    if ($results) {

        $sql = "SELECT id, naam FROM lid WHERE id = $lidId";
        $result = $mysqli->query($sql);
        $row = $result->fetch_assoc();

        $session->flashSession('lid_bewerken', "Uw naam is gewijzigd.");

    } else {
        print 'Naam wijzigen mislukt! : (' . $mysqli->errno . ') ' . $mysqli->error;
    }

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>
<main class="container">

    <?= $messages->alertMessage('success', $session->flashSession('lid_bewerken')); ?>

    <div class="well">
        <h1>Lid bewerken</h1>
        <p>Wijzig uw naam</p>
    </div>

    <form role="form" method="post" action="">

        <div class="row">
            <div class="col-xs-3">
                <div class="form-group">
                    <label for="name">Naam</label>
                    <input type="text" name="name" class="form-control" id="name" value="<?php echo $row['naam']; ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <button type="submit" name="submit" class="btn btn-success">Opslaan</button>
            </div>
        </div>

    </form>

</main>
</body>
</html>