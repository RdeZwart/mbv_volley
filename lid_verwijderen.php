<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';

require_once 'tools/session.php';
$session = new Session();

$mysqli = get_mysqli();

// Haal het team ID uit het HTTP request
$lidId = 0;
if (isset($_GET['lidid'])) {
    $lidId = $_GET['lidid'];
}

// Attempt delete query execution
$sqlTeamHasLid = "DELETE FROM team_has_lid WHERE lid=" . $lidId;
$sqlLid = "DELETE FROM lid WHERE id=" . $lidId;

$teamHasLid = mysqli_query($mysqli, $sqlTeamHasLid);
$lid        = mysqli_query($mysqli, $sqlLid);

if($teamHasLid && $lid) {

    //Melding zetten in session en deze tonen op lede.php
    $session->flashSession('gebruiker_verwijderd', "Gebruiker " . $lidId . " is verwijderd.");
    //Gebruiker terug sturen naar leden.php
    header('Location: leden.php');
    exit;

} else {
    echo "ERROR: verwijderen niet gelukt." . mysqli_error($mysqli);
}

mysqli_close($mysqli);
    
?>