<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Session klassen aanroepen en init
require_once 'tools/Session.php';
$session = new Session();

// controleer of uitloggen
if (isset($_GET['action']) && $_GET['action'] === 'logoff') {
    session_destroy();
    //Een header sturen
    header('Location: wedstrijdschema.php');
    exit;
}

$loginTry = isset($_POST['userid']) && isset($_POST['passwd']);
// Controleer op inloggen
if ($loginTry) {
    $username = strip_tags($_POST['userid']);
    $passwd = strip_tags($_POST['passwd']);
    if (authenticateUser($username, $passwd)) {

        //welkomsbericht klaar zetten voor ingelogde gebruiker
        $session->flashSession('weclome', "Welkom op de website " . getAuthenticatedUsername());

        //Een header sturen
        header('Location: wedstrijdschema.php');
        exit;
    }
}


// anders form laten zien
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>
<main class="container">

    <?php
    if ($loginTry && !isAuthenticated()) {
        echo '<span class="error">Login niet succesvol. Probeer opnieuw</span>';
    }
    ?>


    <div class="row">
        <div class="col-xs-6 col-md-4"></div>

        <div class="col-xs-6 col-md-4">

            <div class="well">

                <h1>Inloggen</h1>

                <form role="form" method="post" action="login.php">
                    <div class="form-group">
                        <label for="userid">Gebruikersnaam</label>
                        <input type="text" class="form-control" name="userid" id="userid" placeholder="Gebruikersnaam"
                               tabindex="1">
                    </div>
                    <div class="form-group">
                        <label for="passwd">Password</label>
                        <input type="password" class="form-control" name="passwd" id="passwd" placeholder="Password"
                               tabindex="2">
                    </div>
                    <button type="submit" class="btn btn-default" tabindex="3">Inloggen</button>
                </form>

            </div>
                        
        </div>

        <div class="col-xs-6 col-md-4"></div>
    </div>


</main>
</body>
</html>
