<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

// Session klassen aanroepen en init
require_once 'tools/Session.php';
$session = new Session();

require_once 'tools/Messages.php';
$messages = new Messages();

//Gebruiker registreren
//Controleren of de gebruiker op de submit knop heeft geklikt
if (isset($_POST['submit'])) {

    //Veld naam genaamd name in de variable $formName plaatsen en validatie erop toepassen.
    $formName = strip_tags(trim($_POST['name']));

    $sql = "INSERT INTO lid (naam) VALUES (?)";

    /* Statement voorbereiden */
    $stmt = $mysqli->prepare($sql);

    /* Bind parameters */
    $stmt->bind_param('s', $formName);

    /* Execute statement */
    if ($stmt->execute()) {

        $stmtId = $stmt->insert_id;

        $sql = "SELECT * FROM lid WHERE id = " . $stmtId;

        if ($result = $mysqli->query($sql)) {

            /* fetch associative array */
            while ($row = $result->fetch_assoc()) {
                $session->flashSession('lid_registreren', $row['naam'] . " is toegevoegd in het systeem.");
            }

            /* free result set */
            $result->free();

        }

        $stmt->close();
    } else {
        echo 'Er is iets fout gegaan, probeer het later opnieuw.';
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>
<main class="container">

    <?= $messages->alertMessage('success', $session->flashSession('lid_registreren')); ?>

    <div class="well">
        <h1>Lid registreren</h1>
        <p>Voeg een lid toe.</p>
    </div>

    <form role="form" method="post" action="">

        <div class="row">
            <div class="col-xs-3">
                <div class="form-group">
                    <label for="name">Naam</label>
                    <input type="text" name="name" class="form-control" id="name">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <button type="submit" name="submit" class="btn btn-success">Lid registreren</button>
            </div>
        </div>

    </form>

</main>
</body>
</html>
