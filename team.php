<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

// Haal het team ID uit het HTTP request
$teamid = 0;
if (isset($_GET['teamid'])) {
    $teamid = $_GET['teamid'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>

<?php include 'header.php' ?>

<main class="container">

    <?php
    $sql = "SELECT * FROM team WHERE id = " . $teamid;
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $teamnaam = $row['naam'];
        echo '<div class="well"><h1>Team ' . $teamid . ': ' . $teamnaam . '</h1></div>';
    } ?>

    <div role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#spelers" aria-controls="spelers" role="tab" data-toggle="tab">De spelers</a></li>
            <li role="presentation"><a href="#wedstrijden" aria-controls="wedstrijden" role="tab" data-toggle="tab">Wedstrijden</a></li>
            <li role="presentation"><a href="#uitslagen" aria-controls="uitslagen" role="tab" data-toggle="tab">Uitslagen</a></li>
            <li role="presentation"><a href="#statistieken" aria-controls="statistieken" role="tab" data-toggle="tab">Statistieken</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="spelers">
                <?php // De spelers van dit team
                $sql = "SELECT l.naam FROM lid l, team_has_lid tl WHERE l.id = tl.lid AND tl.team = " . $teamid;
                $resSpelers = $mysqli->query($sql);
                if (!$resSpelers || $resSpelers->num_rows == 0) {
                    echo '<div class="alert alert-warning" role="alert">' .
                        '<i class="fa fa-exclamation-triangle"></i> Er zijn geen spelers in dit team</div>';
                } else { ?>
                    <table class="table table-striped">
                    <?php while ($rowSpeler = $resSpelers->fetch_assoc()) { ?>
                        <tr>
                            <td class="col-sm-1">
                                <i class="fa fa-user fa-3x"></i>
                            </td>
                            <td class="col-sm-11">
                                <strong><?php echo $rowSpeler['naam'] ?></strong><br/>
                                <small>Al 3 sezoenen actief</small>
                            </td>
                        </tr>
                    <?php } ?>
                    </table>
                <?php } ?>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="wedstrijden">

                <?php // De wedsrtijden van dit team (team A, Team B of Scheidsrechter

                $sql = "SELECT s.datum, w.tijd, w.veld, tb.naam " .
                    "FROM w_team wta, wedstrijd w, w_team wtb, team tb, speelweek s " .
                    "WHERE wta.teamid=" . $teamid . " AND wta.rol=1 AND wta.wedstrijdid=w.id " .
                    "AND wtb.wedstrijdid=w.id AND wtb.rol=2 AND wtb.teamid=tb.id " .
                    "AND s.id=w.speelweekid " .
                    "UNION " .
                    "SELECT s.datum, w.tijd, w.veld, ta.naam " .
                    "FROM w_team wtb, wedstrijd w, w_team wta, team ta, speelweek s " .
                    "WHERE wtb.teamid=" . $teamid . " AND wtb.rol=2 AND wtb.wedstrijdid=w.id " .
                    "AND wta.wedstrijdid=w.id AND wta.rol=1 AND wta.teamid=ta.id " .
                    "AND s.id=w.speelweekid " .
                    "UNION " .
                    "SELECT s.datum, w.tijd, w.veld, '<i>Scheidsrechter</i>' AS naam " .
                    "FROM w_team wts, wedstrijd w, speelweek s " .
                    "WHERE wts.teamid=" . $teamid . " AND wts.rol=3 " .
                    "AND wts.wedstrijdid=w.id " .
                    "AND s.id=w.speelweekid " .
                    "ORDER BY datum, tijd";

                $resWedstr = $mysqli->query($sql);
                if (!$resWedstr || $resWedstr->num_rows == 0) {
                    echo '<div class="alert alert-info" role="alert">' .
                        '<i class="fa fa-info-circle"></i> Er zijn geen wedstrijden voor dit team</div>';
                } else {
                ?>
                <table class="table table-striped">
                    <tr>
                        <th>Datum</th>
                        <th>Tijd</th>
                        <th>Veld</th>
                        <th>Tegen</th>
                    </tr>
                    <?php while ($rowWedstrijd = $resWedstr->fetch_assoc()) { ?>
                        <tr>
                            <td class="col-sm-1">
                                <?php echo $rowWedstrijd['datum'] ?><br/>
                            </td>
                            <td class="col-sm-1">
                                <?php echo $rowWedstrijd['tijd'] ?><br/>
                            </td>
                            <td class="col-sm-1">
                                <?php echo $rowWedstrijd['veld'] ?><br/>
                            </td>
                            <td class="col-sm-11">
                                <strong><?php echo $rowWedstrijd['naam'] ?></strong><br/>
                            </td>
                        </tr>
                    <?php } ?>
                    </table>
                    <?php } ?>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="uitslagen">Hier komen de uitslagen</div>
            <div role="tabpanel" class="tab-pane fade" id="statistieken">

                <?php // De spelers van dit team

                $sql = "SELECT * FROM statsview WHERE team = " . $teamid;
                $resStats = $mysqli->query($sql);

                if (!$resStats || $resStats->num_rows == 0) {
                    echo '<div class="alert alert-warning" role="alert">' . '<i class="fa fa-exclamation-triangle"></i> Er zijn geen statistieken in dit team</div>';
                } else {
                    $rowStats = $resStats->fetch_assoc();
                    ?>
                    <table class="table table-striped">
                        <tr>
                            <td>
                                <strong>W</strong><br/>
                                <small><i>Aantal gespeelde wedstrijden</i></small>
                            </td>
                            <td>
                                <strong><?php echo $rowStats['W'] ?></strong><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>P</strong><br/>
                                <small><i>Competitiepunten (gewonnen sets min strafpunten)</i></small>
                            </td>
                            <td>
                                <strong><?php echo $rowStats['P'] ?></strong><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Sv</strong><br/>
                                <small><i>Scores voor</i></small>
                            </td>
                            <td>
                                <strong><?php echo $rowStats['Sv'] ?></strong><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>St</strong><br/>
                                <small><i>Scores tegen</i></small>
                            </td>
                            <td>
                                <strong><?php echo $rowStats['St'] ?></strong><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>S</strong><br/>
                                <small><i>Score saldo (Sv - St)</i></small>
                            </td>
                            <td>
                                <strong><?php echo $rowStats['S'] ?></strong><br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Str.P</strong><br/>
                                <small><i>Strafpunten</i></small>
                            </td>
                            <td>
                                <strong><?php echo $rowStats['strp'] ?></strong><br/>
                            </td>
                        </tr>
                    </table>
                <?php } ?>
            </div>

        </div>

    </div>

</main>
</body>
</html>
 