<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Session klassen aanroepen en init
require_once 'tools/Session.php';
$session = new Session();

require_once 'tools/Messages.php';
$messages = new Messages();

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>
<main class="container">

    <?= $messages->alertMessage('success', $session->flashSession('gebruiker_verwijderd')); ?>

    <div class="well">
        <h1>Teams</h1>
        <p>Een overzicht van alle teams</p>
        <a href="teamregistreren.php" type="button" class="btn btn-default">Team toevoegen</a>
    </div>
    <?php
    $sql = "SELECT * FROM team";
    $resWeken = $mysqli->query($sql);

    if ($resWeken->num_rows == 0) {

        echo '<div class="alert alert-warning" role="alert">' . '<i class="fa fa-exclamation-triangle"></i> Er zijn geen teams gevonden</div>';

    } elseif($result = $mysqli->query($sql)) { ?>

        <!-- fetch associative array -->
        <table class="table table-condensed table-striped">

          <tr>
              <th>Klasse</th>
              <th>Naam</th>
              <th>Bewerken</th>
          </tr>

          <?php while ($row = $result->fetch_assoc()) { ?>
              <tr>
                <td><?php echo $row['klasse']; ?></td>
                <td><?php echo $row['naam']; ?></td>
                <td><a href="aanpassenteams.php?wedstrijdid=<?php echo $row['id']; ?>">Aanpassen</a></td>
              </tr>
          <?php } ?>

        </table>


        <?php
        /* free result set */
        $result->free();

    } else {
        echo 'Geen resultaat.';
    }

    /* close connection */
    $mysqli->close();

    ?>

</main>
</body>
</html>
