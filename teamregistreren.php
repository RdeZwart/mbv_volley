<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

// Session klassen aanroepen en init
require_once 'tools/Session.php';
$session = new Session();

require_once 'tools/Messages.php';
$messages = new Messages();

//Gebruiker registreren
//Controleren of de gebruiker op de submit knop heeft geklikt
if (isset($_POST['submit'])) {

    //Veld naam genaamd name in de variable $formName plaatsen en validatie erop toepassen.
    $formTeam = strip_tags(trim($_POST['name']));
    $formKlasse = strip_tags(trim($_POST['klasse']));

    $sql = "INSERT INTO team (klasse, naam) VALUES (?, ?)";

    /* Statement voorbereiden */
    $stmt = $mysqli->prepare($sql);

    /* Bind parameters */
    $stmt->bind_param('ss', $formKlasse, $formTeam);

    /* Execute statement */
    if ($stmt->execute()) {

        $stmtId = $stmt->insert_id;

        $sql = "SELECT * FROM team WHERE id = " . $stmtId;

        if ($result = $mysqli->query($sql)) {

            /* fetch associative array */
            while ($row = $result->fetch_assoc()) {
                $session->flashSession('team_registreren', $row['naam'] . " is toegevoegd in het systeem.");
            }

            /* free result set */
            $result->free();

        }

        $stmt->close();
    } else {
        echo 'Er is iets fout gegaan, probeer het later opnieuw.';
    }

}

$sqlKlas = "SELECT * FROM klas ORDER by code ASC";
$resultKlas = $mysqli->query($sqlKlas);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBV Volley</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>
<main class="container">

    <?= $messages->alertMessage('success', $session->flashSession('team_registreren')); ?>

    <div class="well">
        <h1>Team registreren</h1>
        <p>Voeg een team toe.</p>
    </div>

    <form role="form" method="post" action="">

        <div class="row">
            <div class="col-xs-3">

                <div class="form-group">
                    <label for="name">Naam</label>
                    <input type="text" name="name" class="form-control" id="name">
                </div>

            </div>
        </div>


        <div class="row">
            <div class="col-xs-3">
                <div class="form-group">

                    <label for="klasse">Klasse</label>

                    <select name="klasse" id="klasse">
                        <option value="">-</option>

                        <?php while ($row = $resultKlas->fetch_assoc()) { ?>
                            <option value="<?= $row['code']; ?>"><?= $row['naam']; ?></option>
                        <?php } ?>

                    </select>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                <button type="submit" name="submit" class="btn btn-success">Team registreren</button>
            </div>
        </div>

    </form>

</main>
</body>
</html>
