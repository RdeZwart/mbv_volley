<?php
/**
 * Created by PhpStorm.
 * User: nathanael
 * Date: 11/10/2017
 * Time: 22:52
 */

class Messages {

    public function alertMessage($alertType, $message) {
        if ($alertType != '' && $message != '') {
            return '<div class="alert alert-dismissible alert-' . $alertType . '" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' . $message . '</div>';
        }
    }

}