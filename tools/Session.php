<?php
/**
 * Created by PhpStorm.
 * User: nathanael
 * Date: 11/10/2017
 * Time: 22:34
 */

class Session {

    function __construct() {

    }

    /**
     * Retrieve the current session by the key that is defined
     * as a parameter in this function. If they key does not
     * exist it will return false.
     *
     * @param string $name
     * @return bool
     */
    public function getSession($name) {
        //controleerd of de parameter $name waarde heeft
        if (isset($name) && !empty($name)) {
            //controleerd of de sessie bestaat
            if ($this->existSession($name)) {
                //returned de session met de opgegeven parameter $name
                return $_SESSION[$name];
            }
        }
        return FALSE;
    }

    /**
     * Maak een sessie aan
     * @param $name
     * @param $value
     * @return mixed
     */
    public function setSession($name, $value) {
        //controleerd of de parameter $name en $value waarde heeft
        if (isset($name) && !empty($name) && isset($value) && !empty($value)) {
            //returned een nieuwe session
            return $_SESSION[$name] = $value;
        }
    }

    /**
     * Controleren of de sessie bestaat
     * @param $name
     * @return bool
     */
    public function existSession($name) {
        //controleerd of de parameter $name waarde heeft
        if (isset($name) && !empty($name)) {
            //controleerd of de SESSION[$name] waarde heeft
            if (isset($_SESSION[$name])) {
                return TRUE;
            }
            return FALSE;
        }
    }

    /**
     * Sessie verwijderen
     * @param $name
     */
    public function unsetSession($name) {
        //controleerd of de parameter $name waarde heeft
        if (isset($name) && !empty($name)) {
            //controleerd of de sessie bestaad met de parameter $name
            if ($this->existSession($name)) {
                //verwijdert de sessie met de parameter $name
                unset($_SESSION[$name]);
            }
        }
    }

    /**
     * Sessie tijdelijk aanmaken en direct weer verwijderen. (tijdelijke meldingen)
     * @param $name
     * @param string $value
     * @return bool|string
     */
    public function flashSession($name, $value = '') {
        //controleerd of de parameter $name waarde heeft
        if (isset($name) && !empty($name)) {
            //controleerd of de sessie met de parameter $name bestaat
            if ($this->existSession($name)) {
                //haalt de sessie op met de parameter $name
                $session = $this->getSession($name);
                //verwijdert de sessie met de parameter $name
                $this->unsetSession($name);
                return $session;
            }else {
                //Maakt een nieuwe sessie aan
                $this->setSession($name, $value);
            }
            //Als de $name wel waarde heeft maar de sessie bestaat niet wordt er niks gereturned.
            return '';
        }
    }

}