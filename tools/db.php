<?php

$url = 'localhost';
$userid = 'root';
$password = '';
$database = 'volleyball';

$mysqli = new mysqli ( $url, $userid, $password, $database );

if ($mysqli->connect_errno) {
	die ( "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error );
}

function get_mysqli() {
    global $mysqli;
    return $mysqli;
}