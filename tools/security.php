<?php
session_start();

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
/**
 * Controleert of het request is gedaan door een geauthentiseerde gebruiker
 * @return TRUE als gebruiker een geauthentiseerde gebruiker is, anders FALSE
 */
function isAuthenticated() {
    if (!isset($_SESSION)) {
        return false;
    }
    return isset($_SESSION['loggedin']);
}

/**
 * Retourneert de gebruikersnaam van de geauthentiseerde gebruiker, anders een
 * waarschuwingsbericht.
 * @return e gebruikersnaam van de geauthentiseerde gebruiker, anders een
 * waarschuwingsbericht.
 */
function getAuthenticatedUsername() {
    if (!isAuthenticated()) {
        return "<USER NOT AUTHENTICATED>";
    }
    return $_SESSION['username'];
}

/**
 * Authenticeert de gebruiker door meegestuurde logingegevens te vergelijken met
 * het gebruikersgegevensbestand. Als gegevens overeenstemmen, dan worden enkele
 * sessievariabelen geset (en eventueel een sessie gestart).
 * @param $username te controleren gebruikersnaam
 * @param $password te controleren wachtwoord
 * @return TRUE als gebruiker is geauthentiseerd, FALSE als niet
 */
function authenticateUser($username, $password) {
    $mysqli = get_mysqli();
    $sql = "SELECT * FROM gebruiker WHERE userid='$username'";
    $userdata = $mysqli->query($sql);
    if ($userdata->num_rows > 0) {
        $user = $userdata->fetch_assoc();
        if (password_verify($password, $user['password'])) {
            $_SESSION['loggedin'] = true;
            $_SESSION['username'] = $username;
            return true;
        }
    }
    return false;
}

/**
 * Zorgt ervoor dat de gebruiker niet meer geaunthenticeerd is in deze sessie.
 */
function unauthenticateUser() {
    unset($_SESSION['loggedin']);
    unset($_SESSION['username']);
    //session_destroy();
}


?>