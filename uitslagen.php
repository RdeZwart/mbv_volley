<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

// Ophalen uitslagen
$sqlUitslagId = "SELECT wedstrijd FROM uitslag_set GROUP BY wedstrijd";
$resId = $mysqli->query($sqlUitslagId);

$counter = 0;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MBV Volley</title>
        <?php include 'head.html' ?>		
    </head>
    <body>
        <?php include 'header.php' ?>
        <main class="container">	
            <div class="well"><h1>Uitslagen</h1></div>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">

                <?php
                if ($resId->num_rows > 0) {
                    $expanded = " in";
                    while ($uitslagId = $resId->fetch_assoc()) {
                        $panelID = 'heading' . $uitslagId['wedstrijd'];
                        $collapseID = 'collapse' . $uitslagId['wedstrijd'];

                        $wedstrijdId = $uitslagId['wedstrijd'];
                        $sqlUitslag = "SELECT * FROM uitslag_set WHERE wedstrijd = $wedstrijdId";
                        $resUitslag = $mysqli->query($sqlUitslag);

                        $sqlTeamA = "SELECT t.naam FROM team as t JOIN wedstrijd as w ON t.id=w.team_a WHERE w.id = $wedstrijdId";
                        $resTeamA = $mysqli->query($sqlTeamA);
                        $sqlTeamB = "SELECT t.naam FROM team as t JOIN wedstrijd as w ON t.id=w.team_b WHERE w.id = $wedstrijdId;";
                        $resTeamB = $mysqli->query($sqlTeamB);

                        ?>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="<?php echo $panelID ?>">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $collapseID ?>" 
                                       aria-expanded="false" aria-controls="<?php echo $collapseID ?>">
                                        Wedstrijd <?php
                                        echo $uitslagId['wedstrijd'];
                                        
                                        while ($teamA = $resTeamA->fetch_assoc()) {

                                            echo "<b> Team A: </b>" . " " . $teamA['naam'];
                                        }

                                        while ($teamB = $resTeamB->fetch_assoc()) {

                                            echo "<b> Team B: </b>" . " " . $teamB['naam'];
                                        }
                                        ?>

                                    </a>
                                </h4>
                            </div>
                            <div id="<?php echo $collapseID ?>" class="panel-collapse collapse<?php echo $expanded ?>" role="tabpanel" 
                                 aria-labelledby="<?php echo $panelID ?>">
                                <div class="panel-body">
                                    <?php
                                    if (!$resUitslag || $resUitslag->num_rows == 0) {

                                        echo '<div class="alert alert-info" role="alert">' .
                                        '<i class="fa fa-info-circle"></i> Er zijn geen wedstrijden gevonden</div>';
                                    } else { ?>

                                        <table class='table table-condensed table-striped'>
                                        <tr>
                                        <td>Set</td>
                                        <td>Score Team A</td>
                                        <td>Score Team B</td>
                                        <td>Punten Team A</td>
                                        <td>Punten Team B</td>
                                        </tr>

                                        <?php while ($uitslag = $resUitslag->fetch_assoc()) {
                                            $counter++;?>
                                            <tr>
                                            <td><?php echo $counter; ?></td>
                                            <td><?php echo $uitslag['score_a']; ?></td>
                                            <td><?php echo $uitslag['score_b']; ?></td>
                                            <td><?php echo $uitslag['punten_a']; ?></td>
                                            <td><?php echo $uitslag['punten_b']; ?></td>
                                            </tr>
                                       <?php } ?>

                                        </table>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $expanded = "";
                    }
                }
                ?>
            </div>

        </main>
    </body>
</html>
