<?php
// Sessies en Autorisatie
require_once 'tools/security.php';
if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // even gemakkelijk, zonder validatie of andere checks
    // PAS OP!!! Gevaarlijk voor SQL-injectie of andere aanvallen en foutgevoelig
    $wedstrijdid = $_POST['wedstrijdid'];

    // Wie is team A?
    $sql = "SELECT * FROM wedstrijd WHERE id=" . $wedstrijdid;
    $result = $mysqli->query($sql);
    if ($result->num_rows == 0) {
        die("Wedstrijd wedstrijdid is niet bekend");
    }
    $wedstrijddata = $result->fetch_assoc();

    $sql = "DELETE FROM uitslag_set WHERE wedstrijd = $wedstrijdid";
    $result = $mysqli->query($sql);
    
    //relocate
    header('location: wedstrijdschema.php');
    exit;
}

$teamid = 0;
if (isset($_GET['wedstrijdid'])) {
    $wedstrijdid = $_GET['wedstrijdid'];
    $sql = "SELECT w.*, s.datum, r.tijd  FROM wedstrijd w 
                INNER JOIN speelweek s ON s.id=w.speelweek 
                INNER JOIN ronde r ON r.id=w.ronde WHERE w.id=$wedstrijdid";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
        $wedstrijddata = $result->fetch_assoc();
        // Haal gegevens van team a er bij
        $team_a_id = $wedstrijddata['team_a'];
        $team_a = $mysqli->query("SELECT * FROM team WHERE id=$team_a_id")->fetch_assoc();
        // ... en stop dit in het resultaat array
        $wedstrijddata['teama'] = $team_a['naam'];
        // Samen met de klasse (is voor alle drie de teams gelijk)
        $wedstrijddata['klasse'] = $team_a['klasse'];
        // Haal gegevens van team b er bij ...
        $team_b_id = $wedstrijddata['team_b'];
        $team_b = $mysqli->query("SELECT * FROM team WHERE id=$team_b_id")->fetch_assoc();
        // ... en stop dit in het resultaat array
        $wedstrijddata['teamb'] = $team_b['naam'];
        // Haal gegevens van het scheidsrechter team er bij ...
        $team_s_id = $wedstrijddata['scheids'];
        $team_s = $mysqli->query("SELECT * FROM team WHERE id=$team_s_id")->fetch_assoc();
        // ... en stop dit in het resultaat array
        $wedstrijddata['teams'] = $team_s['naam'];
    }
}


// Ophalen uitslagen
$sqlUitslagId = "SELECT wedstrijd FROM uitslag_set GROUP BY wedstrijd";
$resId = $mysqli->query($sqlUitslagId);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MBV Volley</title>
        <?php include 'head.html' ?>		
    </head>
    <body>
        <?php include 'header.php' ?>
        <main class="container">	
            <div class="well"><h1>Verwijderen uitslag</h1></div>
            <div class="mymessage alert alert-danger">
                <?php echo "Weet u zeker dat u deze uitslagen wilt verwijderen?"; ?>
            </div>
            <form method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-4"><strong>DATUM:</strong> <?php echo $wedstrijddata['datum'] ?></div>
                            <div class="col-xs-4"><strong>TIJD:</strong> <?php echo $wedstrijddata['tijd'] ?></div>
                            <div class="col-xs-2"><strong>VELD:</strong> <?php echo $wedstrijddata['veld'] ?></div>
                            <div class="col-xs-2"><strong>KLAS:</strong> <?php echo $wedstrijddata['klasse'] ?></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-xs-12"><strong>SCHEIDSRECHTER:</strong> <?php echo $wedstrijddata['teams'] ?></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-xs-6"><strong>TEAM A:</strong> <?php echo $wedstrijddata['teama'] ?></div>
                            <div class="col-xs-6"><strong>TEAM B:</strong> <?php echo $wedstrijddata['teamb'] ?></div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="wedstrijdid" value="<?php echo $wedstrijddata['id'] ?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th rowspan="2">SET</th>
                                    <th colspan="2">SCORE</th>
                                    <th colspan="2">PUNTEN</th>
                                </tr>
                                <tr>
                                    <th>TEAM A</th>
                                    <th>TEAM B</th>
                                    <th>TEAM A</th>
                                    <th>TEAM B</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $counter = 0;
                                $sql = "SELECT * FROM uitslag_set WHERE wedstrijd = $wedstrijdid";
                                $resUitslag = $mysqli->query($sql);
                                while ($uitslag = $resUitslag->fetch_assoc()) {
                                    $counter++;
                                    ?>
                                    <tr>
                                        <th><strong><?php echo $counter; ?></strong></th>
                                        <th><?php echo $uitslag['score_a']; ?></th>
                                        <th><?php echo $uitslag['score_b']; ?></th>
                                        <th><?php echo $uitslag['punten_a']; ?></th>
                                        <th><?php echo $uitslag['punten_b']; ?></th>

                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-danger">Verwijderen</button>
                    </div>
                </div>                
            </form>
        </main>
    </body>
</html>
