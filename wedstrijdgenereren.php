<?php
require_once 'tools/security.php';

if (!isAuthenticated()) {
    header("HTTP/1.1 403 Unauthorized");
    header("Location: 403.php");
    exit;
}

require_once 'tools/db.php';

$mysqli =  get_mysqli();
?>

<html>
    <head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>
    </head>

    <body>
        <?php include 'header.php' ?>
		<main class="container">

        <div style="text-align: center;">
        <h3> Om te beginnen moet de database eerst gecleared worden.</h3>
            <h3>Daarna kun je het schema genereren</h3>

            <form method="post">
            <input type="submit" class="btn btn-success" name="generate" id="generate" value="Genereer wedstrijdschema" >
            <input type="submit" class="btn btn-danger" name="clear" id="clear" value="Clear de database" >
            </form>
        </div>


<?php 

// clear de teams en scheids uit de wedstrijd database
function clearGenerate($mysqli){
     $sql = "UPDATE wedstrijd SET team_a = null, team_b = null, scheids = null";
     $result = $mysqli->query($sql); ?>
<div class="bg-success alert" role="alert">
    <?php echo "database gecleared <br />"; ?>
</div>
<?php
}

//function om de wedstrijden te genereren
function generateSchedule($mysqli)
    {
        ?>
        <div class="bg-success alert" role="alert">
        <?php echo "Het is gelukt om het schema te genereren <br />"; ?>
     </div>
     <?php
        // variablen om te gebruiken in de loops
        $veldIndex = 1;
        $i = 1;
        $veld = 1;
        // per klasse wordt er een loop doorlopen en worden de goede ploegen tegen elkaar te plaatsen
        for($klasseIndex = 1; $klasseIndex<=4; $klasseIndex++ ){
            if( $klasseIndex == 1) {
                $KI = "H1";
            }
            if( $klasseIndex == 2){
                $KI = "D1";
            }
            if( $klasseIndex == 3){
                $KI = "H2";
            }
            if( $klasseIndex == 4){
                $KI = "D2";
            }
        //ophalen van de team ID's
        $arrTeams = array();
        $sqlTeam = "SELECT * FROM team WHERE klasse = '$KI'" ;
        $resultTeam = $mysqli->query($sqlTeam);
        while($rowteam =$resultTeam->fetch_assoc()){
        $arrTeams[] = $rowteam['id']; }


        //algoritme om het schema in te vullen

        $n = count($arrTeams); //aantal ronden
        $rounds = $n - 1; //ploegen kunnen niet tegen zichzelf spelen als het goed is
        //for loop om iedere ronde in te vullen
        for ($r = 1; $r <= $rounds; $r++) {
            if (($n/2)<=$i){
                $i=1;
            }
            // while loop om de wedstrijden te genereren            
            while($i <= ($n/2)){
                // for loop die de ronde per speelweek controleert
                for ($j = 1; $j <= 3; $j++){
                    if($KI =="H1"){
                        if( $veldIndex == 1 || $veldIndex == 2){
                            $veld = 2;
                        }
                        if($veldIndex == 3){
                            $veldIndex = 1;
                        }
                        if($i >= ($n/2-1)){
                            $veldIndex++;
                        }
                    }
                    else if($KI =="D1"){
                        if( $veldIndex == 3 || $veldIndex == 4){
                            $veld = 4;
                        }
                        if($veldIndex == 5){
                            $veldIndex = 3;
                        }
                        if($i >= ($n/2-1)){
                            $veldIndex++;
                        }
                    }
                        //while loop die de Heren 1 klasse in velden 1/2 plaatst en Dames 1  in velden 3/4
                        while($veldIndex <= $veld){
                        
                            $homeIndex = ($i==1)? 1 : (($r+$i-2) % ($n-1) +2);
                            $awayIndex = ($n - 1 + $r - $i) % ($n -1) +2 ;
                            $scheidsIndex = ($n - 3 + $r - $i) % ($n -1) +2;
                            if ($r % 2 == 0) {
                            $swap = $homeIndex;
                            $homeindex = $awayIndex;
                            $awayindex = $swap;
                            }
                            if ($arrTeams[$homeIndex-1] != 0 && $arrTeams[$awayIndex-1] != 0 && $arrTeams[$scheidsIndex-1] != 0) 
                            {
                                $team_a = $arrTeams[$homeIndex-1];
                                $team_b = $arrTeams[$awayIndex-1];
                                $scheids = $arrTeams[$scheidsIndex-1];
                            }
                            // update query om de gegenereerde wedstrijden aan de database toe te voegen                            
                            $sql = "UPDATE wedstrijd
                            SET team_a = $team_a , team_b = $team_b, scheids = $scheids
                            WHERE speelweek = $r && veld = $veldIndex && ronde = $j";
                            $result = $mysqli->query($sql);

                            $i++;
                            $veldIndex++;
                                       
                        }
                }

            } 
        }

    } 

}

if(array_key_exists('generate',$_POST)){
   generateSchedule($mysqli);
}
if(array_key_exists('clear',$_POST)){
    clearGenerate($mysqli);
}
?>    
</main>
</body>
</html>

