<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Session klassen aanroepen en init
require_once 'tools/Session.php';
$session = new Session();

require_once 'tools/Messages.php';
$messages = new Messages();

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli = get_mysqli();

// Speeltijden ophalen
$tijden = Array();
$sql = "SELECT * FROM ronde";
$resTijden = $mysqli->query($sql);
while ($rowTijd = $resTijden->fetch_assoc()) {
    $tijden[$rowTijd['id']] = $rowTijd['tijd'];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>MBV Volley</title>
    <?php include 'head.html'; ?>
</head>

<body>
<?php include 'header.php' ?>
<main class="container">

    <?= $messages->alertMessage('success', $session->flashSession('weclome')); ?>

    <div class="well"><h1>Wedstrijdschema</h1></div>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">

        <?php

        //get speelweek and place it in panel
        $sql = "SELECT * FROM speelweek ORDER BY datum DESC";

        $resWeken = $mysqli->query($sql);
        if ($resWeken->num_rows == 0) {
            echo '<div class="alert alert-warning" role="alert">' . '<i class="fa fa-exclamation-triangle"></i> Er zijn geen speelweken gevonden</div>';
        } else {
            $expanded = " in";
            while ($rowWeek = $resWeken->fetch_assoc()) {
                $date = date("d F Y", strtotime($rowWeek['datum']));
                $panelID = 'heading' . $rowWeek['id'];
                $collapseID = 'collapse' . $rowWeek['id'];
                ?>

                <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="<?php echo $panelID ?>">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $collapseID ?>"
                               aria-expanded="false" aria-controls="<?php echo $collapseID ?>">
                                Speelweek <?php echo $rowWeek['id'] . ": " . $date ?>
                            </a>
                        </h4>
                    </div>

                    <div id="<?php echo $collapseID ?>" class="panel-collapse collapse<?php echo $expanded ?>"
                         role="tabpanel"
                         aria-labelledby="<?php echo $panelID ?>">
                        <div class="panel-body">
                            <?php
                            $speelweek = $rowWeek['id'];
                            $sql = "SELECT wedstrijd.*, team.klasse FROM wedstrijd
                                                        INNER JOIN team ON wedstrijd.team_a = team.id WHERE speelweek = $speelweek";
                            $resWedstr = $mysqli->query($sql);
                            if (!$resWedstr || $resWedstr->num_rows == 0) {
                                ?>
                                <table class="table table-condensed table-striped">
                                    <tr>
                                        <th class="col-sm-1">Tijd</th>
                                        <th class="col-sm-1">Veld</th>
                                        <th class="col-sm-1">Klas</th>
                                        <th class="col-sm-3">Team A</th>
                                        <th class="col-sm-3">Team B</th>
                                        <th class="col-sm-2">Scheidsrechter/teller</th>
                                        <th class="col-sm-1">Klas</th>
                                        <th></th>
                                    </tr>
                                    <?php while ($rowWedstr = $resWedstr->fetch_assoc()) { ?>
                                        <tr>
                                            <td><?php echo $tijden[$rowWedstr['ronde']]; ?></td>
                                            <td><?php echo $rowWedstr['veld']; ?></td>
                                        </tr>
                                    <?php }
                                    ?>
                                </table>
                                <div class="alert alert-info" role="alert">
                                    <i class="fa fa-info-circle"></i> Er zijn geen wedstrijden gevonden
                                </div>
                            <?php } else {
                                ?>
                                <table class="table table-condensed table-striped">
                                    <tr>
                                        <th class="col-sm-1">Tijd</th>
                                        <th class="col-sm-1">Veld</th>
                                        <th class="col-sm-1">Klas</th>
                                        <th class="col-sm-3">Team A</th>
                                        <th class="col-sm-3">Team B</th>
                                        <th class="col-sm-2">Scheidsrechter/teller</th>
                                        <th class="col-sm-1">Klas</th>

                                        <?php if (isAuthenticated()): ?>
                                            <th class="col-sm-1">Uitslag</th>
                                        <?php endif; ?>

                                    </tr>
                                    <?php while ($rowWedstr = $resWedstr->fetch_assoc()) { ?>
                                        <tr>
                                            <td><?php echo $tijden[$rowWedstr['ronde']]; ?></td>
                                            <td><?php echo $rowWedstr['veld']; ?></td>
                                            <td><?php echo $rowWedstr['klasse']; ?></td>
                                            <td><?php echo $rowWedstr['team_a']; ?></td>
                                            <td><?php echo $rowWedstr['team_b']; ?></td>
                                            <td><?php echo $rowWedstr['scheids']; ?></td>
                                            <td><?php echo $rowWedstr['klasse']; ?></td>

                                            <?php if (isAuthenticated()) { ?>

                                                <td>
                                                    <?php
                                                    // Kijken of er nog geen uitslag is
                                                    $wedstrijd_id = $rowWedstr['id'];
                                                    $sql = "SELECT COUNT(*) AS uitslag FROM uitslag_set WHERE wedstrijd=$wedstrijd_id";
                                                    $res = $mysqli->query($sql);
                                                    $row = $res->fetch_assoc();
                                                    if ($row['uitslag'] == 0) {
                                                        echo '<a href="invullenuitslag.php?wedstrijdid=' . $wedstrijd_id . '"><i class="fa fa-plus" aria-hidden="true"></i></a>';
                                                    }
                                                    if ($row['uitslag'] >= 1) {
                                                        echo '<a href="aanpassenuitslag.php?wedstrijdid=' . $wedstrijd_id . '"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                                                        echo '<a href="verwijderenuitslag.php?wedstrijdid=' . $wedstrijd_id . '"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                                                    }
                                                    ?>
                                                </td>

                                            <?php } // sluit if is authenticated ?>

                                        </tr>
                                    <?php }
                                    ?>
                                </table>
                            <?php } // end if ?>
                        </div>
                    </div>
                </div>

                <?php
                $expanded = "";
            }
        }
        ?>

</main>
</body>
</html>
